That is the cygwin port of the debian apt tool, as part of the cygany
software suite. Cygany is a debian/apt overlay over cygwin, enabling
a better, debian-like package administration, multi-tenancy and
non-interactive package installers.

Project is a fork of the debian apt repository from the salsa.debian.org .

I use the following remote repos for development: (`git remote -v` output)

origin	git@gitlab.com:cygany/apt.git (fetch)
origin	git@gitlab.com:cygany/apt.git (push)
upstream	https://salsa.debian.org/apt-team/apt.git (fetch)
upstream	https://salsa.debian.org/apt-team/apt.git (push)

And the local branches are set up on this way:

* cygany   db7a8232c [origin/cygany] compiles and works in cygwin
  main     c555d8f1a [upstream/main] Merge branch 'fix-959093' into 'main'

In the current state of the cygany development, it only compiles with a

```
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules build binary
```

command, but later it will be better.
